import Axios from "axios";

export abstract class Api {
  private static axios = Axios.create();

  static async getLocation(): Promise<string>{
    const response = await this.axios.get('http://localhost:8787/location');
    return response.data.country;
  }
}