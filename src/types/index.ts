
export interface Job{
    company: string;
    city: string;
    jobTitle: string;
    url: string;
    from: Date;
    to?: Date;
    quote?: string;
}

export interface Education{
    facility: string;
    city: string;
    educationTitle: string;
    from: Date;
    to?: Date;
}


export interface Project{
    title: string;
    keywords: string[];
    description: string;
    skills?: string[];
    company: string;
    url?: string;
    urlDisplay?: string;
}

export interface Personal{
    name: string;
    homebase: string;
    age: number;
    email: string;
    currentLocation?: string;
    summary: string;
    github?: string;
    gitlab?: string;
    linkedin?: string;
}

export interface Skill{
    name: string;
    keywords?: string[];
    yearsOfExperience: number;
    level?: SkillLevel;
}

export interface Tool{
    name: string;
    url?: string;
}

export interface Book{
    title: string;
    subtitle?: string;
    author: string;
    url?: string;
}


export enum SkillLevel{
    Beginner = 1,
    Elementary,
    Intermediate,
    Advanced,
    Expert,

}